import smtplib
import sys
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime

sender_email = "massmailer@novideasoft.com"
receiver_email = "niv.barchel@novideasoft.com"
password = str(sys.argv[1])
current_month = str(datetime.now().month - 1)
current_year = str(datetime.now().year)
monthsdict = {
  "1": "ינואר",
  "2": "פברואר",
  "3": "מרץ",
  "4": "אפריל",
  "5": "מאי",
  "6": "יוני",
  "7": "יולי",
  "8": "אוגוסט",
  "9": "ספטמבר",
  "10": "אוקטובר",
  "11": "נובמבר",
  "12": "דצמבר"
}

current_month = monthsdict[current_month]
message = MIMEMultipart("alternative")
bcc = ['Dganit@NAPA.CO.IL', 'Naftali@napa.co.il', 'moran@fsfp.co.il', 'mor_r@fsfp.co.il', 'support_napa@novideasoft.com', 'support_ibi@novideasoft.com']
message["Subject"] = f" עודכנו הנתונים לחודש {current_month}"
message["From"] = "Novidea IT"
message["To"] = receiver_email
message['Bcc'] = ', '.join(bcc)

html = f"""\
<html dir="rtl">
  <body>
   <p><span style='font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;'>שלום רב,<br><br>עודכנו נתוני הסימולטור הפיננסי עבור:</span><br><span style='font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;'>{current_month} / {current_year}</span></p>
<p><span style="font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif;">בברכה,&nbsp;</span></p>
<p><span style='font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;'>מחלקת IT<br>Novidea</span></p>
  </body>
</html>
"""


part2 = MIMEText(html, "html")
message.attach(part2)

# Create secure connection with server and send email
context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(
        sender_email, receiver_email, message.as_string()
    )